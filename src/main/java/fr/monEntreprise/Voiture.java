package fr.monEntreprise;

import java.util.List;

public class Voiture {
    private String modele;
    private Personne conducteur;

    public Voiture(String modele) {
        this.modele = modele;
    }

    @Override
    public String toString() {
        return this.modele;
    }

    public String getModele() {
        return modele;
    }

    public void setModele(String modele) {
        this.modele = modele;
    }

    public void setConducteur(Personne conducteur) {
        this.conducteur = conducteur;
    }

    public void detruireVoiture(List<Voiture> voitures) {
        this.conducteur.setVoiture(null);
        voitures.remove(this);
    }
}

