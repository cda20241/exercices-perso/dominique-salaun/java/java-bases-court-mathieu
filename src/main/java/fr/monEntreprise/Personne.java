package fr.monEntreprise;

public class Personne {
    private String prenom;
    private String nom;
    private Integer age;
    private Voiture voiture;

    public Personne(String prenom, String nom) {
        this.prenom = prenom;
        this.nom = nom;
    }

    public Personne(String prenom, String nom, Integer age) {
        this.prenom = prenom;
        this.nom = nom;
        this.age = age;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public String getNom() {
        return this.nom;
    }

    public Integer getAge() {
        return this.age;
    }

    public Voiture getVoiture() {
        return voiture;
    }

    public void setPrenom(String p_prenom) {
        this.prenom = p_prenom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setVoiture(Voiture voiture) {
        this.voiture = voiture;
    }

    public void setAge(Integer age) {
        if (age > 0) {
            this.age = age;
        }
    }

    public void acheterVoiture(Voiture p_voiture) {
        this.voiture = p_voiture;
        this.voiture.setConducteur(this);
    }

}

