package fr.monEntreprise;

import java.util.ArrayList;
import java.util.List;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        // Press Alt+Entrée with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.

        Personne michel = new Personne("michel", "toto", 40);
        Voiture kangoo = new Voiture("Kangoo");
        Voiture megane = new Voiture("Mégane");
        List<Voiture> voitures = new ArrayList();
        voitures.add(kangoo);
        voitures.add(megane);

        michel = null;
        Personne jacques = new Personne("jacques", "toto");
        // jacques achète une voiture
        jacques.acheterVoiture(kangoo);
        System.out.println("Voiture de Jacques : " + jacques.getVoiture());
        // puis jacques
        kangoo.detruireVoiture(voitures);
        System.out.println("Voiture de Jacques : " + jacques.getVoiture());
        System.out.println(jacques.getPrenom());
    }
}

